package au.com.foo.media.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import au.com.foo.media.dto.Article;
import au.com.foo.media.dto.MediaSummary;
import au.com.foo.media.dto.Store;
import au.com.foo.media.exception.DuplicateIdException;
import au.com.foo.media.exception.IdNotFoundException;

class ArticleServiceTest {
	ArticleService articleService=null;
	Article article=null;
	@BeforeEach
	void setUp() throws Exception {
		articleService = new ArticleService();
		article=new Article();
	}

	@AfterEach
	void tearDown() throws Exception {
		articleService=null;
		article=null;
		Store.articleStore.clear();
		Store.dateIdStore.clear(); 
	}

	//initialization methods
	private Article getArticle(String id) {
		List<String> lst = new ArrayList<String>();
		article.setId(id);
		article.setDate("2018-08-14");
		article.setBody("body");
		article.setTitle("title");
		lst.add("a");
		article.setTags(lst);
		return article;
	}

	private Article getArticle(String id, List<String> tags) {
		Article temp=new Article();
		temp.setId(id);
		temp.setDate("2018-08-14");
		temp.setBody("body");
		temp.setTitle("title");
		temp.setTags(tags);
		return temp;
	}
	
	private Article getArticle(String id, String date) {
		Article temp=new Article();
		List<String> lst = new ArrayList<String>();
		temp.setId(id);
		temp.setDate(date);
		temp.setBody("body");
		temp.setTitle("title");
		lst.add("a");
		lst.add("b");
		temp.setTags(lst);
		return temp;
	}

	
	//POST method Tests
	@Test
	void testSuccessfullyAddingOfArticleToStore() throws DuplicateIdException {
		articleService.add(getArticle("1"));
		Article articleFromStore = Store.articleStore.get(article.getId());
		
		assertEquals(article.getId(), articleFromStore.getId());
		assertEquals(article.getTitle(), articleFromStore.getTitle());
		assertEquals(article.getBody(), articleFromStore.getBody());
		assertEquals(article.getDate(), articleFromStore.getDate());
		assertEquals(article.getTags().get(0), articleFromStore.getTags().get(0));
	}
	
	@Test
	void testSuccessfullyAddingOfDateAndIdToStore() throws DuplicateIdException {
		articleService.add(getArticle("2"));
		List<String> lst = Store.dateIdStore.get(article.getDate().toString().replaceAll("-", ""));
		assertEquals(lst.get(0), article.getId());
	}
	
	@Test
	void testDuplicateIdExceptionIsThrown() {
		boolean exceptionCaught = false;
		try {
			articleService.add(getArticle("3"));
			articleService.add(getArticle("3"));
		} catch (DuplicateIdException e) {
			exceptionCaught = true;
		}
		if(!exceptionCaught)
			fail("Duplicate ID insertion test failed");
	}
	
	//GET Requests Tests
	@Test
	void testGetMethodIfItReturnsArticle() throws DuplicateIdException, IdNotFoundException {
	
		Article orgArt = getArticle("1");
		articleService.add(orgArt);
		
		Article art = Store.articleStore.get("1");
		assertEquals(orgArt.getId(), art.getId());
		assertEquals(orgArt.getDate(), art.getDate());
		assertEquals(orgArt.getBody(), art.getBody());
		assertEquals(orgArt.getTitle(), art.getTitle());
	}
	
	@Test
	void testGetMethodIfItReturnsMediaSummaryForCommonTag() throws DuplicateIdException, IdNotFoundException {
		List<String> lst = new ArrayList<>();
		lst.add("a");
		lst.add("b");
		articleService.add(getArticle("1", lst));
		List<String> lst2 = new ArrayList<>();
		lst2.add("a");
		lst2.add("c");
		articleService.add(getArticle("2", lst2));
		MediaSummary summary = articleService.get("20180814", "a");
		assertEquals(summary.getCount(), 2);
		assertEquals(summary.getIdList().get(0), "1");
		assertEquals(summary.getIdList().get(1), "2");
		assertEquals(summary.getRelated_tags().get(0), "b");
		assertEquals(summary.getRelated_tags().get(1), "c");
		assertEquals(summary.getTag(), "a");
	}
	
	@Test
	void testGetMethodIfItReturnsMediaSummarForDifferentTag() throws DuplicateIdException, IdNotFoundException {
		List<String> lst = new ArrayList<>();
		lst.add("a");
		lst.add("b");
		articleService.add(getArticle("1", lst));
		List<String> lst2 = new ArrayList<>();
		lst2.add("a");
		lst2.add("c");
		articleService.add(getArticle("2", lst2));
		MediaSummary summary = articleService.get("20180814", "b");
		assertEquals(summary.getCount(), 1);
		assertEquals(summary.getIdList().get(0), "1");
		assertEquals(summary.getRelated_tags().get(0), "a");
		assertEquals(summary.getTag(), "b");
	}

	//This will be caught at request level, however tested the internal working to  check if system crashes due to NULL tags
	@Test
	void testGetMethodIfItReturnsMediaSummaryForNullTag() throws DuplicateIdException, IdNotFoundException {
		List<String> lst = null;
		articleService.add(getArticle("1", lst));
		MediaSummary summary = articleService.get("20180814", "b");
		assertEquals(summary.getCount(), 0);
		assertEquals(summary.getIdList().size(), 0);
		assertEquals(summary.getRelated_tags().size(), 0);
		assertEquals(summary.getTag(), "b");
	}
	
	//This will be caught at request level, however tested the internal working to  check if system crashes due to input NULL tag in get request
	@Test
	void testGetMethodIfItReturnsMediaSummaryForNullInputTag() throws DuplicateIdException, IdNotFoundException {
		List<String> lst = new ArrayList<>();
		lst.add("a");
		lst.add("b");
		articleService.add(getArticle("1", lst));
		MediaSummary summary = articleService.get("20180814", null);
		assertEquals(summary.getCount(), 0);
		assertEquals(summary.getIdList().size(), 0);
		assertEquals(summary.getRelated_tags().size(), 0);
		assertEquals(summary.getTag(), null);
	}
	
	//This will be caught at request level, however tested the internal working to check if system crashes due to invalid date
	@Test
	void testGetMethodIfItReturnsMediaSummaryForNullInputDate() throws DuplicateIdException, IdNotFoundException {
		
		articleService.add(getArticle("1", ""));
		try {
			articleService.get("20180814", "a");
		}
		catch (Exception e) {
			System.out.println(e.getClass());
		}
		
	}
	
	@Test
	void testGetMethodIfItReturnsMediaSummaryForDifferentDates() throws DuplicateIdException, IdNotFoundException {
		
		articleService.add(getArticle("1", "2018-08-14"));
		articleService.add(getArticle("2", "2018-08-15"));
		MediaSummary summary = articleService.get("20180814", "a");
		assertEquals(summary.getCount(), 1);
		assertEquals(summary.getIdList().get(0), "1");
		assertEquals(summary.getRelated_tags().get(0), "b");
		assertEquals(summary.getTag(), "a");
	}
	
	
	@Test
	void testGetMethodThrowsIdNotFoundException() throws DuplicateIdException {
		boolean exceptionCaught = false;
		articleService.add(getArticle("1", "2018-08-14"));
		articleService.add(getArticle("2", "2018-08-15"));
		try {
			articleService.get("3");
		}catch (Exception IdNotFoundException) {
			exceptionCaught = true;
		}
		if(!exceptionCaught)
			fail("Exception not caught for Invalid-Id Get-Request");
	}
	
	@Test
	void testGet() {
		//fail("Not yet implemented");
	}

}
