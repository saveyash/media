package au.com.foo.media;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
@ComponentScan({"au.com.foo.media"})
@SpringBootConfiguration
@EnableAutoConfiguration
public class MediaApp {
	public static void main(String[] args) {
		SpringApplication.run(MediaApp.class, args);
	}
}
