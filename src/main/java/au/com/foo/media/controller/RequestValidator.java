package au.com.foo.media.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.com.foo.media.dto.Article;
import au.com.foo.media.exception.ValidationException;

@Component
public class RequestValidator {
	@Autowired
	private ValidationException exception;
	
	public void validate(Article article) throws ValidationException {
		if((article.getId() == null) || (article.getId().trim().length() <= 0) ) {
			exception.setMessage("Validation failed due to bad request - Invalid ID, record not updated");
			throw exception;
		}

	    DateFormat dtF = new SimpleDateFormat("yyyy-MM-dd");
	    try {
	        dtF.parse(article.getDate());
	    } catch (ParseException e) {
			exception.setMessage("Validation failed due to bad request - Invalid Date or Date format, record not updated");
			throw exception;
	    }
	    
		if ((article.getTags().size() <=0)) {
			exception.setMessage("Validation failed due to bad request - Missing Tags, record not updated");
			throw exception;
		}
		
		for(String tag : article.getTags()) {
			if((tag == null) || (tag.trim().length() <= 0)) {
				exception.setMessage("Validation failed due to bad request - Invalid Tag, record not updated");
				throw exception;
			}
		}
	}
	
	public void validate(String id) throws ValidationException {
		if ((id == null) || (id.equals("")) || (id.trim().length() <= 0)) {
			exception.setMessage("Validation failed due to bad request, records can not be fetched");
			throw exception;
		}
	}
	
	public void validate(String date, String tag) throws ValidationException {
		if ((date == null)              || 
			(date.equals(""))           || 
			(date.trim().length() <= 0) ||
			(date.trim().length() > 8)  ||
			(!date.matches("[0-9]+"))	) {
			exception.setMessage("Validation failed due to missing date or wrong date-format, records can not be fetched");
			throw exception;
		}
		if ((tag == null) || (tag.equals("")) || (tag.trim().length() <= 0)) {
			exception.setMessage("Validation failed due to missing tag, records can not be fetched");
			throw exception;
		}
	}
}
