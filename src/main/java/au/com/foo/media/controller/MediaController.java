package au.com.foo.media.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.com.foo.media.dto.Article;
import au.com.foo.media.dto.MediaSummary;
import au.com.foo.media.exception.DuplicateIdException;
import au.com.foo.media.exception.IdNotFoundException;
import au.com.foo.media.exception.ValidationException;
import au.com.foo.media.service.ArticleService;

@RestController
public class MediaController {
	@Autowired
	private ArticleService articleService;
	@Autowired
	private RequestValidator validator;
	
	@RequestMapping(method=RequestMethod.POST,  value="/articles", consumes = "application/json", produces = "application/json")
	public  ResponseEntity<HttpStatus> addArticle(@RequestBody Article article) throws DuplicateIdException, ValidationException {
		validator.validate(article);
	    return articleService.add(article);
	}
	
	@RequestMapping(method=RequestMethod.GET,  value="/articles/{id}", consumes = "application/json", produces = "application/json")
	public Article getArticle (@PathVariable String id) throws IdNotFoundException, ValidationException {
		validator.validate(id);
		return articleService.get(id);
	}
	
	@RequestMapping(method=RequestMethod.GET,  value="/tag/{tagName}/{date}", consumes = "application/json", produces = "application/json")
	public MediaSummary getTagSummary (@PathVariable String tagName, @PathVariable String date) throws IdNotFoundException, ValidationException {
		validator.validate(date, tagName);
		return articleService.get(date, tagName);
	}
}
