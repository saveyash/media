package au.com.foo.media.exception;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import au.com.foo.media.exception.ErrorDetails;

@ControllerAdvice
public class MediaException {

	@ExceptionHandler(FileNotFoundException.class)
	public ResponseEntity<ErrorDetails> handleFileNotFoundException(Exception ex, WebRequest request){
		 ErrorDetails errorDetails = new ErrorDetails(HttpStatus.NOT_FOUND.toString() , ex.getMessage(),request.getDescription(false), new Date());
		 return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(IOException.class)
	public ResponseEntity<ErrorDetails> handleIOException(Exception ex, WebRequest request){
	 ErrorDetails errorDetails = new ErrorDetails(HttpStatus.NOT_FOUND.toString() , ex.getMessage(),request.getDescription(false), new Date());
	 return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(DuplicateIdException.class)
	  public ResponseEntity<ErrorDetails> duplicateId(DuplicateIdException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(HttpStatus.CONFLICT.toString() , ex.getMessage(),request.getDescription(false), new Date());
	    return new ResponseEntity<>(errorDetails, HttpStatus.CONFLICT);
	  }
	
	@ExceptionHandler(ValidationException.class)
	  public ResponseEntity<ErrorDetails> validationException(ValidationException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(HttpStatus.NOT_ACCEPTABLE.toString() , ex.getMessage(),request.getDescription(false), new Date());
	    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_ACCEPTABLE);
	  }
	
	@ExceptionHandler(IdNotFoundException.class)
	  public ResponseEntity<ErrorDetails> IdNotFound(IdNotFoundException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(HttpStatus.BAD_REQUEST.toString() , ex.getMessage(),request.getDescription(false), new Date());
	    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	  }
	
	@ExceptionHandler(Exception.class)
	  public ResponseEntity<ErrorDetails> handleAllExceptions(Exception ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(HttpStatus.NOT_FOUND.toString() , ex.getMessage(),request.getDescription(false), new Date());
	    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	  }
}
