package au.com.foo.media.exception;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
@Component 
public class ValidationException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
    private HttpStatus httpcode;
	public String getMessage() {
		return message;
	}

	public HttpStatus getHttpcode() {
		return httpcode;
	}

	public void setHttpcode(HttpStatus httpcode) {
		this.httpcode = HttpStatus.NOT_ACCEPTABLE;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}