package au.com.foo.media.dto;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@Component 
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"tag",
"count",
"articles",
"related_tags"
})
public class TagSummary 
{

@JsonProperty("tag")
private String tag;
@JsonProperty("count")
private int count;
@JsonProperty("articles")
private List<String> articles = null;
@JsonProperty("related_tags")
private List<String> relatedTags = null;
/**
* No args constructor for use in serialization
* 
*/
public TagSummary() {
}

/**
* 
* @param articles
* @param count
* @param relatedTags
* @param tag
*/
public TagSummary(String tag, int count, List<String> articles, List<String> relatedTags) {
super();
this.tag = tag;
this.count = count;
this.articles = articles;
this.relatedTags = relatedTags;
}

@JsonProperty("tag")
public String getTag() {
return tag;
}

@JsonProperty("tag")
public void setTag(String tag) {
this.tag = tag;
}

@JsonProperty("count")
public int getCount() {
return count;
}

@JsonProperty("count")
public void setCount(int count) {
this.count = count;
}

@JsonProperty("articles")
public List<String> getArticles() {
return articles;
}

@JsonProperty("articles")
public void setArticles(List<String> articles) {
this.articles = articles;
}

@JsonProperty("related_tags")
public List<String> getRelatedTags() {
return relatedTags;
}

@JsonProperty("related_tags")
public void setRelatedTags(List<String> relatedTags) {
this.relatedTags = relatedTags;
}

}
