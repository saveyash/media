package au.com.foo.media.dto;


import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@Component 
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"title",
"date",
"body",
"tags"
})
public class Article 
{

@JsonProperty("id")
private String id;
@JsonProperty("title")
private String title;
@JsonProperty("date")
private String date;
@JsonProperty("body")
private String body;
@JsonProperty("tags")
private List<String> tags = null;
/**
* No args constructor for use in serialization
* 
*/
public Article() {
}

/**
* 
* @param tags
* @param id
* @param body
* @param title
* @param date
*/
public Article(String id, String title, String date, String body, List<String> tags) {
super();
this.id = id;
this.title = title;
this.date = date;
this.body = body;
this.tags = tags;
}

@JsonProperty("id")
public String getId() {
return id;
}

@JsonProperty("id")
public void setId(String id) {
this.id = id;
}

@JsonProperty("title")
public String getTitle() {
return title;
}

@JsonProperty("title")
public void setTitle(String title) {
this.title = title;
}

@JsonProperty("date")
public String getDate() {
return date;
}

@JsonProperty("date")
public void setDate(String date) {
this.date = date;
}

@JsonProperty("body")
public String getBody() {
return body;
}

@JsonProperty("body")
public void setBody(String body) {
this.body = body;
}

@JsonProperty("tags")
public List<String> getTags() {
return tags;
}

@JsonProperty("tags")
public void setTags(List<String> tags) {
this.tags = tags;
}

}
