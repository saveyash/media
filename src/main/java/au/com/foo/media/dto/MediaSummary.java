package au.com.foo.media.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
 
public class MediaSummary {
	int count;
	String tag = "";
	List<String> idList = null;
	List<String> related_tagsList = null;
	
	public MediaSummary(){
		
	}
	
	public MediaSummary(String tag){
		this.count = 0;
		this.tag = tag;
		idList = new ArrayList<>();
		related_tagsList = new ArrayList<>();
	}
	
	public int getCount() {
		return count;
	}

	public void incrementCount() {
		this.count++;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public List<String> getIdList() {
		return idList;
	}

	public void addIdList(String id) {
		if(!idList.contains(id))
			idList.add(id);
	}

	public List<String> getRelated_tags() {
		return related_tagsList;
	}

	public void addRelated_tags(List<String> allTags, String searchTag) {
		for(String tag : allTags) {
			if((!related_tagsList.contains(tag)) &&(!tag.equals(searchTag)))
				related_tagsList.add(tag);
		}
	}

	

}
