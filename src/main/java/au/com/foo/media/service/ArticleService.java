package au.com.foo.media.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import au.com.foo.media.dto.Article;
import au.com.foo.media.dto.MediaSummary;
import au.com.foo.media.dto.Store;
import au.com.foo.media.exception.DuplicateIdException;
import au.com.foo.media.exception.IdNotFoundException;

@Service
public class ArticleService {
	
	public synchronized ResponseEntity<HttpStatus> add(Article article) throws DuplicateIdException {
		if (Store.articleStore.get((article.getId()))==null) {
			Store.articleStore.put(article.getId(), article);
			
			String date = article.getDate().toString().replaceAll("-","");
			List<String> idList;
			idList = (Store.dateIdStore.get(date) == null) ? new ArrayList<>(): Store.dateIdStore.get(date); 
				
			idList.add(article.getId());
			Store.dateIdStore.put(date,  idList);
			
		} else {
			DuplicateIdException ex = new DuplicateIdException();
			ex.setMessage("duplicate id found, record not updated");
			throw ex;
		}
		return new ResponseEntity<HttpStatus>(HttpStatus.ACCEPTED);
	}

	public synchronized Article get(String id) throws IdNotFoundException {
		if (Store.articleStore.get(id) ==null) {
			IdNotFoundException ex = new IdNotFoundException();
			ex.setMessage("Matching Id not found, please send matching id");
			throw ex;
		}
		return Store.articleStore.get(id);
	}
	
	public synchronized MediaSummary get(String dt, String tag) throws IdNotFoundException{
		if ((dt == null) || (Store.dateIdStore.get(dt) ==null)) {
			IdNotFoundException ex = new IdNotFoundException();
			ex.setMessage("Matching date not found, please send matching date");
			throw ex;
		}
		return getDateTagValues(dt, tag);
	}

	private synchronized MediaSummary getDateTagValues(String dt, String tag){
		List<String> dateTagList = Store.dateIdStore.get(dt);
		MediaSummary summery = new MediaSummary(tag);
		for(String id: dateTagList) {
			List<String> allTags = Store.articleStore.get(id).getTags();
			if((allTags != null) && (allTags.indexOf(tag) >=0)) {
				summery.addIdList(id);
				summery.addRelated_tags(allTags, tag);
				summery.incrementCount();
			}
		}
		return summery;
	}
}
